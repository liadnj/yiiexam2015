<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "statusExam".
 *
 * @property integer $id
 * @property string $name
 */
class StatusExam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statusExam';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }
	
	public static function getStatus()
	{
	$defultStatus = self::find(2);
	return $defultStatus;
	}
	
	
	
	public static function getStatuses()
	{
		$allStatuses = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($allStatuses, 'id', 'name');
		return $allStatusesArray;						
	}
	
	public static function getStatusesWithAllStatuses()
	{
		$allStatuses = self::getStatuses();
		$allStatuses[-1] = 'All Statuses';
		$allStatuses = array_reverse ( $allStatuses, true );
		return $allStatuses;	
	}
	

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
