<?php

namespace app\models;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryId
 * @property integer $statusId
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'categoryId'], 'required'],
            [['id', 'categoryId', 'statusId'], 'integer'],
           // [['title'], 'string', 'max' => 255],
        ];
    }
	
	 public function rules()
    {
		$rules = []; 
		$stringItems = [['title'], 'string', 'max' => 255];
		$integerItems  = ['id', 'categoryId', 'statusId', 'created_at', 'updated_at', 'created_by', 'updated_by'];		
		if (\Yii::$app->user->can('updateLead')) {
			$integerItems[] = 'owner';
		}
		$integerRule = [];
		$integerRule[] = $integerItems;
		$integerRule[] = 'integer';
		$ShortStringItems = [['name', 'email', 'phone'], 'string', 'max' => 255]; 
		$rules[] = $stringItems;
		$rules[] = $integerRule;
		$rules[] = $ShortStringItems;		
		return $rules;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function getStatusIdItem()
    {
        return $this->hasOne(StatusExam::className(), ['id' => 'statusId']);
    }
	
	public function getCategoryIdItem()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }
	
	
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }
}
