<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
	<?php  if (\Yii::$app->user->isGuest) {
?>
        <h1>My First Crm Application!</h1>
<?} ?>

	<?php  if (!\Yii::$app->user->isGuest) {
?>
        <h1>Wellcome!</h1>
<?} ?>


        <p class="lead">Simple CRM Yii2 - Liad Nizri</p>
<?php  if (\Yii::$app->user->isGuest) {
?>
        
        <p><a class="btn btn-lg btn-success" href="login">Tap To Login</a></p>
<?} ?>
<?php  if (!\Yii::$app->user->isGuest) {
?>
        
        <p><a class="btn btn-lg btn-success" href="lead">Go To Leads</a></p>
<?} ?>
<?php  if (!\Yii::$app->user->isGuest) {
?>
        
        <p><a class="btn btn-lg btn-success" href="user">Go To Users</a></p>
<?} ?>
   </div>

    <div class="body-content">

        <div class="row">
           
          
        </div>

    </div>
</div>
