<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Activity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            //'categoryId',
            //'statusId',
	[
            /*['class' => 'yii\grid\ActionColumn'],
			'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('ActivitySearch[statusId]', $statusId, $statusIdes, ['class'=>'form-control']),
				*/
				
				'attribute' => 'statusId',
				'label' => 'StatusExam',
				'format' => 'raw',
				'value' => function($model){
					return $model->statusIdItem->name;
				},
				'filter'=>Html::dropDownList('ActivitySearch[statusId]', $statusId, $statusIdes, ['class'=>'form-control']),
				],
				
				 [
				'attribute' => 'categoryId',
				'label' => 'Category',
				'format' => 'raw',
				'value' => function($model){
				return $model->categoryIdItem->name;
				},
				'filter'=>Html::dropDownList('ActivitySearch[categoryId]', $categoryId, $categoryIdes, ['class'=>'form-control']),
				],
				
			 ['class' => 'yii\grid\ActionColumn'],
			
			],





					
            
        
    ]); ?>
</div>
